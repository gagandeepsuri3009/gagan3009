# Configure the Azure Provider

provider "azurerm" {
  version = "2.5.0"
  features {}
}

resource "azurerm_resource_group" "testRG" {
  name     = "VMSS-rg"
  location = "West Europe"
}


resource "azurerm_virtual_network" "vnetvmsstest" {
  name                = "testVNET"
  address_space       = ["192.168.1.0/24"]
  location            = azurerm_resource_group.testRG.location
  resource_group_name = azurerm_resource_group.testRG.name
}

resource "azurerm_subnet" "testsubnet" {
  name                 = "AzureBastionSubnet"
  resource_group_name  = azurerm_resource_group.testRG.name
  virtual_network_name = azurerm_virtual_network.vnetvmsstest.name
  address_prefix       = "192.168.1.224/27"
}


resource "azurerm_subnet" "subnetvmss" {
  name                 = "testvmsssubnet"
  resource_group_name  = azurerm_resource_group.testRG.name
  virtual_network_name = azurerm_virtual_network.vnetvmsstest.name
  address_prefix       = "192.168.1.0/27"
}


resource "azurerm_public_ip" "bastionhostpubip" {
  name                = "bastionpubip"
  location            = azurerm_resource_group.testRG.location
  resource_group_name = azurerm_resource_group.testRG.name
  allocation_method   = "Static"
  sku                 = "Standard"
}

resource "azurerm_bastion_host" "testbastionhost" {
  name                = "examplebastion"
  location            = azurerm_resource_group.testRG.location
  resource_group_name = azurerm_resource_group.testRG.name

  ip_configuration {
    name                 = "configuration"
    subnet_id            = azurerm_subnet.testsubnet.id
    public_ip_address_id = azurerm_public_ip.bastionhostpubip.id
  }
}


resource "random_id" "testserver" {
  keepers = {
    azi_id = 1
  }

  byte_length = 8
}


resource "azurerm_traffic_manager_profile" "testtrafficmanager" {
  name                   = random_id.testserver.hex
  resource_group_name    = azurerm_resource_group.testRG.name
  traffic_routing_method = "Priority"

  dns_config {
    relative_name = random_id.testserver.hex
    ttl           = 100
  }

  monitor_config {
    protocol                     = var.protocol_name
    port                         = 80
    path                         = "/"
    interval_in_seconds          = 30
    timeout_in_seconds           = 9
    tolerated_number_of_failures = 3
  }
}

resource "azurerm_traffic_manager_endpoint" "testtrafficmanager-endpoint" {
  name                = random_id.testserver.hex
  resource_group_name = azurerm_resource_group.testRG.name
  profile_name        = azurerm_traffic_manager_profile.testtrafficmanager.name
  type                = "azureEndpoints"
  target_resource_id  = azurerm_public_ip.lbpubip.id
  weight              = 1
}


resource "azurerm_public_ip" "lbpubip" {
  name                = "mypublicip"
  location            = azurerm_resource_group.testRG.location
  resource_group_name = azurerm_resource_group.testRG.name
  allocation_method   = "Static"
  ip_version          = "IPv4"
  domain_name_label   = random_string.fqdn.result
}

resource "random_string" "fqdn" {
  length  = 6
  special = false
  upper   = false
  number  = false
}

resource "azurerm_lb" "LBtest" {
  name                = "testLB"
  location            = azurerm_resource_group.testRG.location
  resource_group_name = azurerm_resource_group.testRG.name

  frontend_ip_configuration {
    name                 = "PublicIPAddress-lb"
    public_ip_address_id = azurerm_public_ip.lbpubip.id
  }
}

resource "azurerm_lb_backend_address_pool" "testbackendpool" {
  resource_group_name = azurerm_resource_group.testRG.name
  loadbalancer_id     = azurerm_lb.LBtest.id
  name                = "BackEndAddressPool1"
}

resource "azurerm_lb_nat_pool" "lbnatpool" {
  resource_group_name            = azurerm_resource_group.testRG.name
  name                           = "ssh"
  loadbalancer_id                = azurerm_lb.LBtest.id
  protocol                       = "Tcp"
  frontend_port_start            = 2222
  frontend_port_end              = 2255
  backend_port                   = 22
  frontend_ip_configuration_name = "PublicIPAddress-lb"
}

resource "azurerm_lb_probe" "testlbprobe" {
  resource_group_name = azurerm_resource_group.testRG.name
  loadbalancer_id     = azurerm_lb.LBtest.id
  name                = "http-probe"
  protocol            = "Http"
  request_path        = "/health"
  port                = 8080
}


resource "azurerm_linux_virtual_machine_scale_set" "testvmss" {
  name                = "test-vmss"
  resource_group_name = azurerm_resource_group.testRG.name
  location            = azurerm_resource_group.testRG.location
  sku                 = "Standard_A2"
  instances           = 2
  admin_username      = "adminuser"

  admin_ssh_key {
    username   = "adminuser"
    public_key = file("~/.ssh/id_rsa.pub")
  }

  source_image_reference {
    publisher = "Canonical"
    offer     = "UbuntuServer"
    sku       = "16.04-LTS"
    version   = "latest"
  }

  os_disk {
    storage_account_type = "Standard_LRS"
    caching              = "ReadWrite"
  }

  network_interface {
    name    = "example"
    primary = true

    ip_configuration {
      name                                   = "internal-test"
      primary                                = true
      subnet_id                              = azurerm_subnet.subnetvmss.id
      load_balancer_backend_address_pool_ids = [azurerm_lb_backend_address_pool.testbackendpool.id]
      load_balancer_inbound_nat_rules_ids    = [azurerm_lb_nat_pool.lbnatpool.id]
    }
  }
}

